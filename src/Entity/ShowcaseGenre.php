<?php
/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 14/2/2019
 * Time: 11:47 AM
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShowcaseGenreRepository")
 * @ORM\Table(name="showcase_genre", indexes={
 *     @Index(name="showcase", columns={"showcase_id"}),
 *     @Index(name="genre", columns={"genre_id"})
 * })
 * @ORM\HasLifecycleCallbacks
 */
class ShowcaseGenre extends BaseEntity
{
    const REPOSITORY = 'App:ShowcaseGenre';

    /**
     * @var Showcase
     *
     * @ORM\ManyToOne(targetEntity="Showcase", inversedBy="showcaseGenre")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="showcase_id", referencedColumnName="id")
     * })
     */
    private $showcase;

    /**
     * @var Genre
     *
     * @ORM\ManyToOne(targetEntity="Genre", inversedBy="showcaseGenre")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="genre_id", referencedColumnName="id")
     * })
     */
    private $genre;

    /**
     * @return Showcase
     */
    public function getShowcase()
    {
        return $this->showcase;
    }

    /**
     * @param Showcase $showcase
     * @return ShowcaseGenre
     */
    public function setShowcase($showcase)
    {
        $this->showcase = $showcase;
        return $this;
    }

    /**
     * @return Genre
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param Genre $genre
     * @return ShowcaseGenre
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;
        return $this;
    }
}