<?php
/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 14/2/2019
 * Time: 11:46 AM
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GenreRepository")
 * @ORM\Table(name="genre")
 * @ORM\HasLifecycleCallbacks
 */
class Genre extends BaseEntity
{
    const REPOSITORY = 'App:Genre';

    /**
     * @var string
     *
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $name;

    /**
     * @var ShowcaseGenre
     *
     * @ORM\OneToMany(targetEntity="ShowcaseGenre", mappedBy="genre")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="genre_id", referencedColumnName="id")
     * })
     */
    private $showcaseGenre;

    /**
     * @return ShowcaseGenre
     */
    public function getShowcaseGenre()
    {
        return $this->showcaseGenre;
    }

    /**
     * @param ShowcaseGenre $showcaseGenre
     * @return Genre
     */
    public function setShowcaseGenre($showcaseGenre)
    {
        $this->showcaseGenre = $showcaseGenre;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Genre
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}