<?php

/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 14/2/2019
 * Time: 10:51 AM
 */
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShowcaseRepository")
 * @ORM\Table(name="showcase")
 * @ORM\HasLifecycleCallbacks
 */
class Showcase extends BaseEntity
{
    const REPOSITORY = 'App:Showcase';

    /**
     * @ORM\Column(type = "text", nullable = true)
     */
    private $body;

    /**
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $cert;

    /**
     * @ORM\Column(type = "smallint", nullable = true)
     */
    private $duration;

    /**
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $headline;

    /**
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $originalId;

    /**
     * @ORM\Column(type = "datetime", nullable = true)
     */
    private $lastUpdated;

    /**
     * @ORM\Column(type = "string", nullable = true)
     */
    private $quote;

    /**
     * @ORM\Column(type = "smallint", nullable = true)
     */
    private $rating;

    /**
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $skyGoId;

    /**
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $skyGoUrl;

    /**
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $sum;

    /**
     * @ORM\Column(type = "text", nullable = true)
     */
    private $synopsis;

    /**
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $url;

    /**
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $year;

    /**
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $class;

    /**
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $sgid;

    /**
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $sgUrl;

    /**
     * @var File
     *
     * @ORM\OneToMany(targetEntity="File", mappedBy="showcase")
     * @OrderBy({"id" = "ASC"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="showcase_id", referencedColumnName="id")
     * })
     */
    private $file;

    /**
     * @var ShowcaseGenre
     *
     * @ORM\OneToMany(targetEntity="ShowcaseGenre", mappedBy="showcase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="showcase_id", referencedColumnName="id")
     * })
     */
    private $showcaseGenre;

    /**
     * @var ShowcasePerson
     *
     * @ORM\OneToMany(targetEntity="ShowcasePerson", mappedBy="showcase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="showcase_id", referencedColumnName="id")
     * })
     */
    private $showcasePerson;

    /**
     * @var ViewingWindow
     *
     * @ORM\OneToMany(targetEntity="ViewingWindow", mappedBy="showcase")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="showcase_id", referencedColumnName="id")
     * })
     */
    private $viewingWindow;

    /**
     * @return ViewingWindow
     */
    public function getViewingWindow()
    {
        return $this->viewingWindow;
    }

    /**
     * @param ViewingWindow $viewingWindow
     * @return Showcase
     */
    public function setViewingWindow($viewingWindow)
    {
        $this->viewingWindow = $viewingWindow;
        return $this;
    }

    /**
     * @return ShowcasePerson
     */
    public function getShowcasePerson()
    {
        return $this->showcasePerson;
    }

    /**
     * @param ShowcasePerson $showcasePerson
     * @return Showcase
     */
    public function setShowcasePerson($showcasePerson)
    {
        $this->showcasePerson = $showcasePerson;
        return $this;
    }

    /**
     * @return ShowcaseGenre
     */
    public function getShowcaseGenre()
    {
        return $this->showcaseGenre;
    }

    /**
     * @param ShowcaseGenre $showcaseGenre
     * @return Showcase
     */
    public function setShowcaseGenre($showcaseGenre)
    {
        $this->showcaseGenre = $showcaseGenre;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     * @return Showcase
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCert()
    {
        return $this->cert;
    }

    /**
     * @param mixed $cert
     * @return Showcase
     */
    public function setCert($cert)
    {
        $this->cert = $cert;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     * @return Showcase
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeadline()
    {
        return $this->headline;
    }

    /**
     * @param mixed $headline
     * @return Showcase
     */
    public function setHeadline($headline)
    {
        $this->headline = $headline;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOriginalId()
    {
        return $this->originalId;
    }

    /**
     * @param mixed $originalId
     * @return Showcase
     */
    public function setOriginalId($originalId)
    {
        $this->originalId = $originalId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * @param mixed $lastUpdated
     * @return Showcase
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuote()
    {
        return $this->quote;
    }

    /**
     * @param mixed $quote
     * @return Showcase
     */
    public function setQuote($quote)
    {
        $this->quote = $quote;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     * @return Showcase
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSkyGoId()
    {
        return $this->skyGoId;
    }

    /**
     * @param mixed $skyGoId
     * @return Showcase
     */
    public function setSkyGoId($skyGoId)
    {
        $this->skyGoId = $skyGoId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSkyGoUrl()
    {
        return $this->skyGoUrl;
    }

    /**
     * @param mixed $skyGoUrl
     * @return Showcase
     */
    public function setSkyGoUrl($skyGoUrl)
    {
        $this->skyGoUrl = $skyGoUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * @param mixed $sum
     * @return Showcase
     */
    public function setSum($sum)
    {
        $this->sum = $sum;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSynopsis()
    {
        return $this->synopsis;
    }

    /**
     * @param mixed $synopsis
     * @return Showcase
     */
    public function setSynopsis($synopsis)
    {
        $this->synopsis = $synopsis;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return Showcase
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     * @return Showcase
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param mixed $class
     * @return Showcase
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSgid()
    {
        return $this->sgid;
    }

    /**
     * @param mixed $sgid
     * @return Showcase
     */
    public function setSgid($sgid)
    {
        $this->sgid = $sgid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSgUrl()
    {
        return $this->sgUrl;
    }

    /**
     * @param mixed $sgUrl
     * @return Showcase
     */
    public function setSgUrl($sgUrl)
    {
        $this->sgUrl = $sgUrl;
        return $this;
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param File $file
     * @return Showcase
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }
}