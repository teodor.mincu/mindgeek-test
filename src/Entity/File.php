<?php
/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 14/2/2019
 * Time: 11:15 AM
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FileRepository")
 * @ORM\Table(name="file", indexes={
 *     @Index(name="showcase", columns={"showcase_id"})
 * })
 * @ORM\HasLifecycleCallbacks
 */
class File extends BaseEntity
{
    const TYPE_CARD = 'cardImages';
    const TYPE_KEY_ART = 'keyArtImages';
    const TYPE_GALLERIES = 'galleries';

    const STORAGE_PATH = 'storage2/images';

    const DOWNLOAD_STATUS_SUCCESS = 'downloaded';
    const DOWNLOAD_STATUS_ERROR = 'error';
    const DOWNLOAD_STATUS_PROCESSING = 'processing';

    const FILE_TYPE_IMAGE = 'image';
    const FILE_TYPE_VIDEO = 'video';


    const REPOSITORY = 'App:File';

    /**
     * @var string
     *
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $type;

    /**
     * @var File
     * @ORM\ManyToOne(targetEntity="File")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $quality;

    /**
     * @var string
     *
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $fileType;

    /**
     * @var integer
     *
     * @ORM\Column(type = "smallint", nullable = true)
     */
    private $height;

    /**
     * @var integer
     *
     * @ORM\Column(type = "smallint", nullable = true)
     */
    private $width;


    /**
     * @var string
     *
     * @ORM\Column(type = "string", nullable = true)
     */
    private $originalId;

    /**
     * @var Showcase
     *
     * @ORM\ManyToOne(targetEntity="Showcase", inversedBy="file")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="showcase_id", referencedColumnName="id")
     * })
     */
    private $showcase;


    /**
     * @var string
     *
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $thumbnailUrl;

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return File
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return File
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getFileType()
    {
        return $this->fileType;
    }

    /**
     * @param string $fileType
     * @return File
     */
    public function setFileType($fileType)
    {
        $this->fileType = $fileType;
        return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return File
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return File
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }


    /**
     * @return Showcase
     */
    public function getShowcase()
    {
        return $this->showcase;
    }

    /**
     * @param Showcase $showcase
     * @return File
     */
    public function setShowcase($showcase)
    {
        $this->showcase = $showcase;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return File
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getThumbnailUrl()
    {
        return $this->thumbnailUrl;
    }

    /**
     * @param string $thumbnailUrl
     * @return File
     */
    public function setThumbnailUrl($thumbnailUrl)
    {
        $this->thumbnailUrl = $thumbnailUrl;
        return $this;
    }

    /**
     * @return int
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param $parent
     * @return $this
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * @param string $quality
     * @return File
     */
    public function setQuality($quality)
    {
        $this->quality = $quality;
        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalId()
    {
        return $this->originalId;
    }

    /**
     * @param string $originalId
     * @return File
     */
    public function setOriginalId($originalId)
    {
        $this->originalId = $originalId;
        return $this;
    }
}