<?php
/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 14/2/2019
 * Time: 11:32 AM
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShowcasePersonRepository")
 * @ORM\Table(name="showcase_person", indexes={
 *     @Index(name="showcase", columns={"showcase_id"}),
 *     @Index(name="person", columns={"person_id"})
 * })
 * @ORM\HasLifecycleCallbacks
 */
class ShowcasePerson extends BaseEntity
{
    const REPOSITORY = 'App:ShowcasePerson';

    const JOB_CAST = 'cast';
    const JOB_DIRECTOR = 'directors';
    const JOB_REVIEW_AUTHOR = 'review_author';

    /**
     * @var string
     *
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $job;

    /**
     * @var Showcase
     *
     * @ORM\ManyToOne(targetEntity="Showcase", inversedBy="showcasePerson")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="showcase_id", referencedColumnName="id")
     * })
     */
    private $showcase;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="showcasePerson")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     * })
     */
    private $person;

    /**
     * @return string
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param string $job
     * @return ShowcasePerson
     */
    public function setJob($job)
    {
        $this->job = $job;
        return $this;
    }

    /**
     * @return Showcase
     */
    public function getShowcase()
    {
        return $this->showcase;
    }

    /**
     * @param Showcase $showcase
     * @return ShowcasePerson
     */
    public function setShowcase($showcase)
    {
        $this->showcase = $showcase;
        return $this;
    }

    /**
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return ShowcasePerson
     */
    public function setPerson($person)
    {
        $this->person = $person;
        return $this;
    }
}