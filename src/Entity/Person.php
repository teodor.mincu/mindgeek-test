<?php
/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 14/2/2019
 * Time: 11:30 AM
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 * @ORM\Table(name="person")
 * @ORM\HasLifecycleCallbacks
 */
class Person extends BaseEntity
{
    const REPOSITORY = 'App:Person';

    /**
     * @var string
     *
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $name;

    /**
     * @var ShowcasePerson
     *
     * @ORM\OneToMany(targetEntity="ShowcasePerson", mappedBy="person")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="showcase_id", referencedColumnName="id")
     * })
     */
    private $showcasePerson;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Person
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ShowcasePerson
     */
    public function getShowcasePerson()
    {
        return $this->showcasePerson;
    }

    /**
     * @param ShowcasePerson $showcasePerson
     * @return Person
     */
    public function setShowcasePerson($showcasePerson)
    {
        $this->showcasePerson = $showcasePerson;
        return $this;
    }
}