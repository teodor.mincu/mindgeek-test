<?php
/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 14/2/2019
 * Time: 12:11 PM
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ViewingWindowRepository")
 * @ORM\Table(name="viewing_window", indexes={
 *     @Index(name="showcase", columns={"showcase_id"})
 * })
 * @ORM\HasLifecycleCallbacks
 */
class ViewingWindow extends BaseEntity
{
    const REPOSITORY = 'App:ViewingWindow';

    /**
     * @var string
     *
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type = "string", nullable = true, length = 255)
     */
    private $wayToWatch;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type = "datetime", nullable = true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type = "datetime", nullable = true)
     */
    private $endDate;

    /**
     * @var Showcase
     *
     * @ORM\ManyToOne(targetEntity="Showcase", inversedBy="viewingWindow")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="showcase_id", referencedColumnName="id")
     * })
     */
    private $showcase;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return ViewingWindow
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getWayToWatch()
    {
        return $this->wayToWatch;
    }

    /**
     * @param string $wayToWatch
     * @return ViewingWindow
     */
    public function setWayToWatch($wayToWatch)
    {
        $this->wayToWatch = $wayToWatch;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     * @return ViewingWindow
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     * @return ViewingWindow
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return ShowcaseGenre
     */
    public function getShowcase()
    {
        return $this->showcase;
    }

    /**
     * @param $showcase
     * @return $this
     */
    public function setShowcase($showcase)
    {
        $this->showcase = $showcase;
        return $this;
    }
}