<?php
/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 18/2/2019
 * Time: 21:29 PM
 */

namespace App\Repository;


use App\Entity\File;
use App\Entity\Gallery;
use App\Entity\Genre;
use App\Entity\Person;
use App\Entity\ShowcaseGenre;
use App\Entity\ShowcasePerson;
use App\Entity\VideoAlternative;
use App\Entity\ViewingWindow;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class ShowcaseRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function findShowcaseListPreview()
    {
        $query = $this->createQueryBuilder('s');
        $query
            ->select(
                's.headline',
                's.id',
                's.synopsis',
                's.year'
            )
            ->orderBy('s.id','asc');

        return $query->getQuery()->getResult();
    }

    /**
     * @param $id
     * @return array
     */
    public function findShowcaseInfo($id)
    {
        $query = $this->createQueryBuilder('s');

        $query
            ->leftJoin(File::REPOSITORY, 'f', Join::WITH, $query->expr()->eq('s.id', 'f.showcase'))
            ->leftJoin(Gallery::REPOSITORY, 'g', Join::WITH, $query->expr()->eq('s.id', 'g.showcase'))
            ->leftJoin(ShowcaseGenre::REPOSITORY, 'sg', Join::WITH, $query->expr()->eq('s.id', 'sg.showcase'))
            ->leftJoin(Genre::REPOSITORY, 'ge', Join::WITH, $query->expr()->eq('ge.id', 'sg.genre'))
            ->leftJoin(ShowcasePerson::REPOSITORY, 'sp', Join::WITH, $query->expr()->eq('s.id', 'sp.showcase'))
            ->leftJoin(Person::REPOSITORY, 'p', Join::WITH, $query->expr()->eq('p.id', 'sp.person'))
            ->leftJoin(VideoAlternative::REPOSITORY, 'va', Join::WITH, $query->expr()->eq('f.id', 'va.file'))
            ->leftJoin(ViewingWindow::REPOSITORY, 'vw', Join::WITH, $query->expr()->eq('vw.showcase', 's.id'))
            ->where(
                $query->expr()->eq('s.id', ':id')
            )
            ->setParameter('id', $id)
            ->orderBy('s.id', 'asc');
        return $query->getQuery()->getResult();
    }
}