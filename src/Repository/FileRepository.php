<?php
/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 19/2/2019
 * Time: 8:01 AM
 */

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class FileRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function findUniqueUrlFiles()
    {
        $query = $this->createQueryBuilder('f');
        $query
            ->where(
                $query->expr()->isNotNull('f.url')
            )
            ->groupBy('f.url');
        return $query->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function findThumbnailUrlFiles()
    {
        $query = $this->createQueryBuilder('f');
        $query
            ->where(
                $query->expr()->isNotNull('f.thumbnailUrl')
            )
            ->groupBy('f.thumbnailUrl');
        return $query->getQuery()->getResult();
    }
}