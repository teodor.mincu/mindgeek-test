<?php

/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 15/2/2019
 * Time: 11:26 AM
 */

namespace App\Service;

use App\Entity\Gallery;
use App\Entity\Genre;
use App\Entity\File;
use App\Entity\Person;
use App\Entity\Showcase;
use App\Entity\ShowcaseGenre;
use App\Entity\ShowcasePerson;
use App\Entity\ViewingWindow;
use App\Repository\FileRepository;
use App\Repository\GenreRepository;
use App\Repository\PersonRepository;
use App\Repository\ShowcaseGenreRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class ShowcaseService
{
    const ID = 'showcase.service';

    /** @var  EntityManagerInterface */
    protected $entityManager;

    /** @var  PersonRepository */
    private $personRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->setEntityManager($entityManager);

        /** @var PersonRepository $personRepository */
        $personRepository = $this->getEntityManager()->getRepository(Person::REPOSITORY);
        $this->setPersonRepository($personRepository);
    }

    public function truncateDatabase()
    {
        $fileRepository = $this->getEntityManager()->getClassMetadata(File::REPOSITORY);
        $genreRepository = $this->getEntityManager()->getClassMetadata(Genre::REPOSITORY);
        $personRepository = $this->getEntityManager()->getClassMetadata(Person::REPOSITORY);
        $showcaseGenreRepository = $this->getEntityManager()->getClassMetadata(ShowcaseGenre::REPOSITORY);
        $showcasePersonRepository = $this->getEntityManager()->getClassMetadata(ShowcasePerson::REPOSITORY);
        $showcaseRepository = $this->getEntityManager()->getClassMetadata(Showcase::REPOSITORY);
        $viewingWindowRepository = $this->getEntityManager()->getClassMetadata(ViewingWindow::REPOSITORY);

        $connection = $this->getEntityManager()->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $connection->query('SET FOREIGN_KEY_CHECKS=0');

        $q = $dbPlatform->getTruncateTableSql($fileRepository->getTableName());
        $connection->executeUpdate($q);
        $q = $dbPlatform->getTruncateTableSql($genreRepository->getTableName());
        $connection->executeUpdate($q);
        $q = $dbPlatform->getTruncateTableSql($personRepository->getTableName());
        $connection->executeUpdate($q);
        $q = $dbPlatform->getTruncateTableSql($showcaseGenreRepository->getTableName());
        $connection->executeUpdate($q);
        $q = $dbPlatform->getTruncateTableSql($showcasePersonRepository->getTableName());
        $connection->executeUpdate($q);
        $q = $dbPlatform->getTruncateTableSql($showcaseRepository->getTableName());
        $connection->executeUpdate($q);
        $q = $dbPlatform->getTruncateTableSql($viewingWindowRepository->getTableName());
        $connection->executeUpdate($q);

        $connection->query('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * @param array $inputShowcaseList
     * @return bool
     */
    public function saveShowcaseList(array $inputShowcaseList)
    {
        /** @var GenreRepository $genreRepository */
        $genreRepository = $this->getEntityManager()->getRepository(Genre::REPOSITORY);

        /** @var ShowcaseGenreRepository $showcaseGenreRepository */
        $showcaseGenreRepository = $this->getEntityManager()->getRepository(ShowcaseGenre::REPOSITORY);

        foreach($inputShowcaseList as $inputShowcase) {
            $showcase = new Showcase();
            $showcase
                ->setBody($this->safeArrayKeyValue($inputShowcase, 'body'))
                ->setCert($this->safeArrayKeyValue($inputShowcase, 'cert'))
                ->setClass($this->safeArrayKeyValue($inputShowcase, 'class'))
                ->setDuration($this->safeArrayKeyValue($inputShowcase, 'duration'))
                ->setHeadline($this->safeArrayKeyValue($inputShowcase, 'headline'))
                ->setOriginalId($this->safeArrayKeyValue($inputShowcase, 'id'))
                ->setLastUpdated($this->safeArrayKeyValue($inputShowcase, 'lastUpdated', 'date'))
                ->setQuote($this->safeArrayKeyValue($inputShowcase, 'quote'))
                ->setRating($this->safeArrayKeyValue($inputShowcase, 'rating'))
                ->setSkyGoId($this->safeArrayKeyValue($inputShowcase, 'skyGoId'))
                ->setSkyGoUrl($this->safeArrayKeyValue($inputShowcase, 'skyGoUrl'))
                ->setSum($this->safeArrayKeyValue($inputShowcase, 'sum'))
                ->setSynopsis($this->safeArrayKeyValue($inputShowcase, 'synopsis'))
                ->setUrl($this->safeArrayKeyValue($inputShowcase, 'url'))
                ->setSgid($this->safeArrayKeyValue($inputShowcase, 'sgid'))
                ->setSgUrl($this->safeArrayKeyValue($inputShowcase, 'sgUrl'))
                ->setYear($this->safeArrayKeyValue($inputShowcase, 'year'));
            unset(
                $inputShowcase['body'],
                $inputShowcase['cert'],
                $inputShowcase['class'],
                $inputShowcase['duration'],
                $inputShowcase['headline'],
                $inputShowcase['id'],
                $inputShowcase['lastUpdated'],
                $inputShowcase['quote'],
                $inputShowcase['rating'],
                $inputShowcase['skyGoId'],
                $inputShowcase['skyGoUrl'],
                $inputShowcase['sum'],
                $inputShowcase['synopsis'],
                $inputShowcase['url'],
                $inputShowcase['sgid'],
                $inputShowcase['sgUrl'],
                $inputShowcase['year']
            );

            $this->getEntityManager()->persist($showcase);

            if(isset($inputShowcase['cardImages']) && is_array($inputShowcase['cardImages']) > 0) {
                $inputShowcase = $this->saveImageList($showcase, $inputShowcase, File::TYPE_CARD);
            }

            if(isset($inputShowcase['keyArtImages']) && is_array($inputShowcase['keyArtImages']) > 0) {
                $inputShowcase = $this->saveImageList($showcase, $inputShowcase, File::TYPE_KEY_ART);
                unset($inputShowcase['keyArtImages']);
            }

            if(isset($inputShowcase['galleries']) && is_array($inputShowcase['galleries']) > 0) {
                $inputShowcase = $this->saveImageList($showcase, $inputShowcase, File::TYPE_GALLERIES);
                unset($inputShowcase['galleries']);
            }

            if(isset($inputShowcase['cast']) && is_array($inputShowcase['cast']) > 0) {
                $inputShowcase = $this->savePersonList($showcase, $inputShowcase, ShowcasePerson::JOB_CAST);
            }

            if(isset($inputShowcase['directors']) && is_array($inputShowcase['directors']) > 0) {
                $inputShowcase = $this->savePersonList($showcase, $inputShowcase, ShowcasePerson::JOB_DIRECTOR);
            }

            if(isset($inputShowcase['reviewAuthor'])) {
                $inputShowcase['review_author'][]['name'] = $inputShowcase['reviewAuthor'];
                $inputShowcase = $this->savePersonList($showcase, $inputShowcase, ShowcasePerson::JOB_REVIEW_AUTHOR);
                unset($inputShowcase['reviewAuthor']);
            }

            if(isset($inputShowcase['genres']) && is_array($inputShowcase['genres']) > 0) {
                foreach($inputShowcase['genres'] as $index => $inputGenre) {
                    $genre = $genreRepository->findOneBy(['name' => $inputGenre]);
                    if (is_null($genre)) {
                        $genre = new Genre();
                        $genre
                            ->setName($inputGenre);
                        $this->getEntityManager()->persist($genre);
                    }
                    /** @var ShowcaseGenre $showcaseGenre */
                    $showcaseGenre = $showcaseGenreRepository->findOneBy([
                        'genre' => $genre,
                        'showcase' => $showcase
                    ]);
                    if(is_null($showcaseGenre)) {
                        $showcaseGenre = new ShowcaseGenre();
                        $showcaseGenre
                            ->setGenre($genre)
                            ->setShowcase($showcase);
                        $this->getEntityManager()->persist($showcaseGenre);
                    }
                    unset(
                        $inputShowcase['genres'][$index]
                    );
                }
                if(count($inputShowcase['genres']) == 0) {
                    unset($inputShowcase['genres']);
                }
            }

            if(isset($inputShowcase['videos']) && is_array($inputShowcase['videos']) > 0) {
                foreach($inputShowcase['videos'] as $index => $inputVideo) {
                    $video = new File();
                    $video
                        ->setFileType(File::FILE_TYPE_VIDEO)
                        ->setTitle($inputVideo['title'])
                        ->setType($inputVideo['type'])
                        ->setUrl($inputVideo['url'])
                        ->setThumbnailUrl($this->safeArrayKeyValue($inputVideo, 'thumbnailUrl'))
                        ->setShowcase($showcase);
                    $this->getEntityManager()->persist($video);
                    $this->getEntityManager()->flush($video);


                    if(isset($inputVideo['alternatives']) && is_array($inputVideo['alternatives']) > 0) {
                        foreach($inputVideo['alternatives'] as $subIndex => $inputVideoAlternative) {
                            $videoAlternative = new File();
                            $videoAlternative
                                ->setFileType(File::FILE_TYPE_VIDEO)
                                ->setQuality($inputVideoAlternative['quality'])
                                ->setUrl($inputVideoAlternative['url'])
                                ->setParent($video)
                                ->setShowcase($showcase);
                            $this->getEntityManager()->persist($videoAlternative);
                            unset(
                                $inputShowcase['videos'][$index]['alternatives'][$subIndex]['quality'],
                                $inputShowcase['videos'][$index]['alternatives'][$subIndex]['url']
                            );
                            if(count($inputShowcase['videos'][$index]['alternatives'][$subIndex]) == 0) {
                                unset($inputShowcase['videos'][$index]['alternatives'][$subIndex]);
                            }
                        }
                        if(count($inputShowcase['videos'][$index]['alternatives']) == 0) {
                            unset($inputShowcase['videos'][$index]['alternatives']);
                        }
                    }
                    unset(
                        $inputShowcase['videos'][$index]['title'],
                        $inputShowcase['videos'][$index]['type'],
                        $inputShowcase['videos'][$index]['url'],
                        $inputShowcase['videos'][$index]['thumbnailUrl']
                    );
                    if(count($inputShowcase['videos'][$index]) == 0) {
                        unset($inputShowcase['videos'][$index]);
                    }
                }
                if(count($inputShowcase['videos']) == 0) {
                    unset($inputShowcase['videos']);
                }
            }

            if(isset($inputShowcase['viewingWindow'])) {
                $viewingWindow = new ViewingWindow();
                $viewingWindow
                    ->setTitle($this->safeArrayKeyValue($inputShowcase['viewingWindow'], 'title'))
                    ->setStartDate($this->safeArrayKeyValue($inputShowcase['viewingWindow'], 'startDate', 'date'))
                    ->setEndDate($this->safeArrayKeyValue($inputShowcase['viewingWindow'], 'endDate', 'date'))
                    ->setWayToWatch($this->safeArrayKeyValue($inputShowcase['viewingWindow'], 'wayToWatch'))
                    ->setShowcase($showcase);
                    $this->getEntityManager()->persist($viewingWindow);
                unset(
                    $inputShowcase['viewingWindow']['title'],
                    $inputShowcase['viewingWindow']['startDate'],
                    $inputShowcase['viewingWindow']['endDate'],
                    $inputShowcase['viewingWindow']['wayToWatch']
                );
                if(count($inputShowcase['viewingWindow']) == 0) {
                    unset($inputShowcase['viewingWindow']);
                }
            }
            if(count($inputShowcase) > 0) {
                return false;
            }
        }
        $this->getEntityManager()->flush();
        return true;
    }

    /**
     * @param $showcase
     * @param $inputShowcase
     * @param $imageType
     * @return mixed
     */
    private function saveImageList($showcase, $inputShowcase, $imageType)
    {
        $downloadStatus = null;
        foreach($inputShowcase[$imageType] as $index => $inputImage) {
            $image = new File();
            $image
                ->setType($imageType)
                ->setFileType(File::FILE_TYPE_IMAGE)
                ->setUrl($this->safeArrayKeyValue($inputImage, 'url'))
                ->setWidth($this->safeArrayKeyValue($inputImage, 'w'))
                ->setHeight($this->safeArrayKeyValue($inputImage, 'h'))
                ->setTitle($this->safeArrayKeyValue($inputImage, 'title'))
                ->setThumbnailUrl($this->safeArrayKeyValue($inputImage, 'thumbnailUrl'))
                ->setOriginalId($this->safeArrayKeyValue($inputImage, 'id'))
                ->setShowcase($showcase);
            $this->getEntityManager()->persist($image);

            unset(
                $inputShowcase[$imageType][$index]['url'],
                $inputShowcase[$imageType][$index]['w'],
                $inputShowcase[$imageType][$index]['h'],
                $inputShowcase[$imageType][$index]['title'],
                $inputShowcase[$imageType][$index]['thumbnailUrl'],
                $inputShowcase[$imageType][$index]['id']
            );
            if(count($inputShowcase[$imageType][$index]) == 0) {
                unset($inputShowcase[$imageType][$index]);
            }
        }
        if(count($inputShowcase[$imageType]) == 0) {
            unset($inputShowcase[$imageType]);
        }
        return $inputShowcase;
    }

    /**
     * @param $showcase
     * @param $inputShowcase
     * @param $job
     * @return bool
     */
    public function savePersonList($showcase, $inputShowcase, $job)
    {
        foreach($inputShowcase[$job] as $index => $inputPerson) {
            if(isset($inputPerson['name'])) {
                $person = $this->getPersonRepository()->findOneBy(['name' => $inputPerson['name']]);
                if (is_null($person)) {
                    $person = new Person();
                    $person
                        ->setName($inputPerson['name']);
                    $this->getEntityManager()->persist($person);
                }

                $showcasePerson = new ShowcasePerson();
                $showcasePerson
                    ->setPerson($person)
                    ->setShowcase($showcase)
                    ->setJob($job);
                $this->getEntityManager()->persist($showcasePerson);

                unset(
                    $inputShowcase[$job][$index]['name']
                );
            }
            if(count($inputShowcase[$job][$index]) == 0) {
                unset($inputShowcase[$job][$index]);
            }
        }
        if(count($inputShowcase[$job]) == 0) {
            unset($inputShowcase[$job]);
        }
        return $inputShowcase;
    }
    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @return ShowcaseService
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    /**
     * @return PersonRepository
     */
    public function getPersonRepository()
    {
        return $this->personRepository;
    }

    /**
     * @param PersonRepository $personRepository
     * @return ShowcaseService
     */
    public function setPersonRepository($personRepository)
    {
        $this->personRepository = $personRepository;
        return $this;
    }

    /**
     * @param $var
     * @param $key
     * @param string $varType
     * @return null
     */
    private function safeArrayKeyValue($var, $key, $varType = 'string')
    {
        if(isset($var[$key])) {
            if($varType == 'date') {
                return new \DateTime($var[$key]);
            }
            return $var[$key];
        }

        return null;
    }
}