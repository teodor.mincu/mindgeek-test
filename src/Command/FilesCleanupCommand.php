<?php
/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 19/2/2019
 * Time: 23:18 PM
 */

namespace App\Command;


use App\Entity\File;
use App\Repository\FileRepository;
use App\Service\ShowcaseService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Unirest;

class FilesCleanupCommand extends Command
{
    /** @var  ShowcaseService */
    private $showcaseService;

    /** @var  FileRepository */
    private $fileRepository;

    /** @var  ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container, ShowcaseService $showcaseService)
    {
        parent::__construct();
        $fileRepository = $container->get('doctrine')->getManager()->getRepository(File::REPOSITORY);
        $this->setFileRepository($fileRepository);
        $this->setShowcaseService($showcaseService);
        $this->setContainer($container);
//        $installmentTaskRepository = $container->get('doctrine')->getManager()->getRepository(InstallmentTask::REPOSITORY);
//        $this->setInstallmentTaskRepository($installmentTaskRepository);
//        $this->setInstallmentTaskService($installmentTaskService);
    }

    protected function configure()
    {
        $this
            ->setName('download:clean-up')
            ->setDescription('Clean downloaded files');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        echo "\n Cleaning after error files \n";
        $start = microtime(true);
        $dirFiles = scandir(dirname(__FILE__) . '/../../storage/');
        $dir = dirname(__FILE__) . '/../../storage/';
        foreach($dirFiles as $item) {
            if($item == '.' || $item == '..') {
                continue;
            }
            $fileContents = file_get_contents($dir . $item);
            if(
                (strpos($fileContents, 'BlobNotFound') > 0) ||
                (strpos($fileContents, '404 Not Found') > 0)
            ){
                unlink($dir . $item);
            }
        }
        echo "\nTime: " . (time() - $start) ." seconds \n";
        echo "\n\nFINISHED\n\n";
        return true;
    }

    /**
     * @return ShowcaseService
     */
    public function getShowcaseService()
    {
        return $this->showcaseService;
    }

    /**
     * @param ShowcaseService $showcaseService
     * @return DataDownloadCommand
     */
    public function setShowcaseService($showcaseService)
    {
        $this->showcaseService = $showcaseService;
        return $this;
    }

    /**
     * @return FileRepository
     */
    public function getFileRepository()
    {
        return $this->fileRepository;
    }

    /**
     * @param FileRepository $fileRepository
     * @return DataDownloadCommand
     */
    public function setFileRepository($fileRepository)
    {
        $this->fileRepository = $fileRepository;
        return $this;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     * @return DataDownloadCommand
     */
    public function setContainer($container)
    {
        $this->container = $container;
        return $this;
    }


}