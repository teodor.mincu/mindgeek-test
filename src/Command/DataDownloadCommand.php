<?php
/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 19/2/2019
 * Time: 23:18 PM
 */

namespace App\Command;


use App\Entity\File;
use App\Repository\FileRepository;
use App\Service\ShowcaseService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Unirest;

class DataDownloadCommand extends Command
{
    /** @var  ShowcaseService */
    private $showcaseService;

    /** @var  FileRepository */
    private $fileRepository;

    /** @var  ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container, ShowcaseService $showcaseService)
    {
        parent::__construct();
        $fileRepository = $container->get('doctrine')->getManager()->getRepository(File::REPOSITORY);
        $this->setFileRepository($fileRepository);
        $this->setShowcaseService($showcaseService);
        $this->setContainer($container);
//        $installmentTaskRepository = $container->get('doctrine')->getManager()->getRepository(InstallmentTask::REPOSITORY);
//        $this->setInstallmentTaskRepository($installmentTaskRepository);
//        $this->setInstallmentTaskService($installmentTaskService);
    }

    protected function configure()
    {
        $this
            ->setName('download:json-data')
            ->setDescription('Download json data');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $start = microtime(true);
        ini_set('max_execution_time', '0');
        $headers = ['Accept' => 'application/json', 'Content-Type' => 'text/html; charset=UTF-8'];

        $response = Unirest\Request::get('https://mgtechtest.blob.core.windows.net/files/showcase.json', $headers);

        $responseBody = utf8_decode($response->raw_body);
        $responseBodyArr = json_decode($responseBody, true);

        if(!is_array($responseBodyArr)) {
            // return invalid data
        }
        if(count($responseBodyArr) == 0) {
            // return empty data
        }

        echo "\n\nTruncating old data from database..";
        $this->getShowcaseService()->truncateDatabase();
        echo "\nTime: " . (time() - $start) . " seconds \n";

        $start = microtime(true);
        echo "\n\nSaving data to database..";
        $this->getShowcaseService()->saveShowcaseList($responseBodyArr);
        echo "\nTime: " . (time() - $start) . " seconds \n";

        echo "\n\nDownloading files..";
        $start = microtime(true);
        /** @var FileRepository $fileRepository */
        $fileList = $this->getFileRepository()->findUniqueUrlFiles();
        $curlUrls = [];
        /** @var File $file */
        foreach($fileList as $file) {
            $curlUrls[] = $file->getUrl();
        }
        $thumbnailFileList = $this->getFileRepository()->findThumbnailUrlFiles();
        foreach($thumbnailFileList as $file) {
            if(!is_null($file->getThumbnailUrl()) && !in_array($file->getThumbnailUrl(), $curlUrls)) {
                $curlUrls[] = $file->getThumbnailUrl();
            }
        }

        $cont = 0;
        $mh = curl_multi_init();
        /** @var File $file */
        foreach($curlUrls as $file) {
            $fp = fopen (dirname(__FILE__) . '/../../storage/'. basename($file), 'w');
            $ch[$cont] = curl_init($file);
            curl_setopt($ch[$cont], CURLOPT_TIMEOUT, 50);
            // write curl response to file
            curl_setopt($ch[$cont], CURLOPT_FILE, $fp);
            curl_setopt($ch[$cont], CURLOPT_FOLLOWLOCATION, true);
            curl_multi_add_handle($mh, $ch[$cont]);
            $cont++;
        }
        $running = 0;
        do {
            curl_multi_exec($mh, $running);
        } while($running > 0);

        echo "\nTime: " . (time() - $start) ." seconds \n";
        echo "\n\nFINISHED\n\n";
        return true;
    }

    /**
     * @return ShowcaseService
     */
    public function getShowcaseService()
    {
        return $this->showcaseService;
    }

    /**
     * @param ShowcaseService $showcaseService
     * @return DataDownloadCommand
     */
    public function setShowcaseService($showcaseService)
    {
        $this->showcaseService = $showcaseService;
        return $this;
    }

    /**
     * @return FileRepository
     */
    public function getFileRepository()
    {
        return $this->fileRepository;
    }

    /**
     * @param FileRepository $fileRepository
     * @return DataDownloadCommand
     */
    public function setFileRepository($fileRepository)
    {
        $this->fileRepository = $fileRepository;
        return $this;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     * @return DataDownloadCommand
     */
    public function setContainer($container)
    {
        $this->container = $container;
        return $this;
    }


}