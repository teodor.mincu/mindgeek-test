<?php

/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 15/2/2019
 * Time: 10:10 AM
 */
namespace App\Controller;

use App\Entity\File;
use App\Entity\Showcase;
use App\Entity\ShowcasePerson;
use App\Repository\ShowcaseRepository;
use App\Service\ShowcaseService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Unirest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class ShowcaseController extends Controller
{
    /**
     * @param Request $request
     * @param ShowcaseService $showcaseService
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route(
     *     "/showcase/index",
     *     name="api_list",
     * )
     */
    public function indexAction(Request $request, ShowcaseService $showcaseService)
    {
        /** @var ShowcaseRepository $showcaseRepository */
        $showcaseRepository = $this->getDoctrine()->getManager()->getRepository(Showcase::REPOSITORY);
        $showcaseList = $showcaseRepository->findShowcaseListPreview();

        return $this->render('showcase-list.html.twig', ['info' => $showcaseList]);
    }

    /**
     * @param Request $request
     * @param ShowcaseService $showcaseService
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route(
     *     "/showcase/show/{id}",
     *     name="api_show",
     * )
     */
    public function showAction(Request $request, ShowcaseService $showcaseService)
    {
        $id = $request->get('id');

        /** @var ShowcaseRepository $showcaseRepository */
        $showcaseRepository = $this->getDoctrine()->getManager()->getRepository(Showcase::REPOSITORY);

        /** @var Showcase $showcase */
        $showcase = $showcaseRepository->findOneBy(['id' => $id]);


        $fileSystem = new Filesystem();

        $cont = 0;
        $showcaseFiles = [];
        /** @var File $file */
        foreach($showcase->getFile() as $file) {
            $showcaseFiles[$cont]['info'] = $file;
            $showcaseFiles[$cont]['exists'] = false;
            if($fileSystem->exists(dirname(__FILE__) . '/../../storage/'. basename($file->getUrl()))) {
                $showcaseFiles[$cont]['exists'] = true;
                $showcaseFiles[$cont]['filename'] = basename($file->getUrl());
                $showcaseFiles[$cont]['thumbnailExists'] = false;
                if(!is_null($file->getThumbnailUrl())) {
                    if($fileSystem->exists(dirname(__FILE__) . '/../../storage/'. basename($file->getThumbnailUrl()))) {
                        $showcaseFiles[$cont]['thumbnailExists'] = true;
                        $showcaseFiles[$cont]['thumbnailFilename'] = basename($file->getThumbnailUrl());
                    }
                }
            }
            $cont++;
        }
        $cast = $directors = [];
        $reviewAuthor = null;
        /** @var ShowcasePerson $showcasePerson */
        foreach($showcase->getShowcasePerson() as $showcasePerson) {
            switch($showcasePerson->getJob()) {
                case ShowcasePerson::JOB_CAST:
                    $cast[] = $showcasePerson->getPerson()->getName();
                    break;
                case ShowcasePerson::JOB_DIRECTOR:
                    $directors[] = $showcasePerson->getPerson()->getName();
                    break;
                case ShowcasePerson::JOB_REVIEW_AUTHOR:
                    $reviewAuthor = $showcasePerson->getPerson()->getName();
                    break;
            }
        }

//        /** @var FileRepository $fileRepository */
//        $fileRepository = $this->getDoctrine()->getManager()->getRepository(File::REPOSITORY);
//        $fileList = $fileRepository->findBy(['showcase' => $showcase->getId()]);
//
//        /** @var GalleryRepository $galeryRepository */
//        $galleryRepository = $this->getDoctrine()->getManager()->getRepository(Gallery::REPOSITORY);
//        $galleryList = $galleryRepository->findBy(['showcase' => $showcase->getId()]);
//
//        $showcaseService->saveShowcaseList($responseBodyArr);
//
//        $genreRepository = $this->getDoctrine()->getRepository()
//
//        /** @var ShowcaseRepository $showcaseRepository */
//        $showcaseRepository = $this->getDoctrine()->getManager()->getRepository(Showcase::REPOSITORY);
//        $showcaseList = $showcaseRepository->findShowcaseListPreview();

        return $this->render('showcase.html.twig', [
            'info' => $showcase,
            'files' => $showcaseFiles,
            'cast_list' => $cast,
            'directors_list' => $directors,
            'review_author' => $reviewAuthor
        ]);
    }

}